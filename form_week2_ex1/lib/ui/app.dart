import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login me',
      home: Scaffold(
        body: LoginScreen(),
      ),
    );
  }

}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }

}

class LoginState extends State<StatefulWidget> {
  final formKey = GlobalKey<FormState>();
  late String email;
  late String lastName;
  late String firstName;
  late int birth;
  late String address;



  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            emailField(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            LastNameField(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            FirstNameField(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            BirthField(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            AddressField(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            loginButton()

          ],
        ),
      ),
    );
  }

  Widget emailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Email address'
      ),
      validator: (value) {
        if (value!.length == 0)  {
          return "please enter information";
        }
        else if (!value.contains('@')) {
          return "Pls input valid email.";
        }
        else if (!value.contains('.')) {
          return "please input valid email.";
        }

        return null;
      },

      onSaved: (value) {
        email = value as String;
      },

    );
  }

  Widget LastNameField() {
    return TextFormField(
      keyboardType: TextInputType.name,
      decoration: InputDecoration(
          icon: Icon(Icons.drive_file_rename_outline),
          labelText: 'First Name'
      ),
      validator: (value) {
        if (value!.length == 0)  {
          return "please enter information";
        }
        else if (value!.length > 30)
          {
            return "can not over 30 words";
          }

        return null;
      },

      onSaved: (value) {
        email = value as String;
      },

    );
  }

  Widget FirstNameField() {
    return TextFormField(
      keyboardType: TextInputType.name,
      decoration: InputDecoration(
          icon: Icon(Icons.drive_file_rename_outline),
          labelText: 'First Name'
      ),
      validator: (value) {
        if (value!.length == 0)  {
          return "please enter information";
        }

        else if (value!.length > 20)
        {
          return "can not over 20 words";
        }

        return null;
      },

      onSaved: (value) {
        email = value as String;
      },

    );
  }

  Widget BirthField() {
    return TextFormField(
      keyboardType: TextInputType.datetime,
      decoration: InputDecoration(
          icon: Icon(Icons.date_range),
          labelText: 'Birthday'
      ),
      validator: (value) {
        RegExp word = RegExp(r'^(?=.*?[0-9])');
        if (value!.length == 0)  {
          return "please enter information";
        }
        else if (!word.hasMatch(value))
        {
          return "must be number";
        }

        return null;
      },

      onSaved: (value) {
        email = value as String;
      },

    );
  }


  Widget AddressField() {
    return TextFormField(
      keyboardType: TextInputType.streetAddress,
      decoration: InputDecoration(
          icon: Icon(Icons.streetview),
          labelText: 'Street Address'
      ),
      validator: (value) {
        if (value!.length == 0)  {
          return "please enter information";
        }

        return null;
      },

      onSaved: (value) {
        email = value as String;
      },

    );
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.save();

            print('email=$email');
            print('lastName=$lastName');
            print('firstName=$firstName');
            print('birth=$birth');
            print('address=$address');
          }
        },
        child: Text('Save')
    );
  }
}



