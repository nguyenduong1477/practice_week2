import 'package:flutter/material.dart';
import '../stream/stream_english.dart';

class App extends StatefulWidget
{
  State<StatefulWidget> createState()
  {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget>
{
  String englishWords = 'Welcome To Most Expensive English Course';
  EnglishWordsStream englishWordsStream = EnglishWordsStream();
  late int count = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Stream Text',

        home: Scaffold
          (
          appBar: AppBar(title: Text('App Bar'),),
          body: Center(
              child:Column(
                  children:[
                    Text(englishWords, style: TextStyle(fontSize: 50)), Text(count.toString(),style: TextStyle(fontSize: 50))
                  ]
              )
          ),

          floatingActionButton: FloatingActionButton
            (
            child: Text('C'),
            onPressed: (){
              changeEnglishWords();
            },
          ),
        )
    );
  }

  changeEnglishWords() async
  {
    englishWordsStream.getEnglishWords().listen((eventEnglishWords) {
      setState(() {
        print(eventEnglishWords);
        englishWords = eventEnglishWords.toLowerCase();
        if (count == eventEnglishWords.length+1)
          count = 1;
        else count +=1;
      });
    });
  }
}