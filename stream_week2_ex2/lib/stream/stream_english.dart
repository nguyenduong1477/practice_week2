import 'dart:html';

import 'package:flutter/material.dart';

class EnglishWordsStream
{

  Stream<String> getEnglishWords() async*
  {
    final List<String> englishWords =
    [
      'Dog',
      'Cat',
      'Mouse',
      'Bear',
      'Fox',
      'Giraffe',
      'Elephant',
      'Lion',
      'Tiger',
      'Hippopotamus',
      'Wolf',
      'Deer',
      'Alligator',
    ];

    yield* Stream.periodic(Duration(seconds: 2), (int t)
    {
      int index = t % 5;
      return englishWords[index];
    });
  }
}